# nuxt-restaurant-ts

Nuxtjs + VueJs port of therestaurant.vercel.app React + Nextjs website. This version adds page transitions and lazy loading images and some performance optimizations. View [Demo](https://vetro.vercel.app/)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Author

[Gagan](https://github.com/devtool-0x01)
