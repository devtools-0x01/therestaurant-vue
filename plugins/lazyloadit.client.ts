import Vue from "vue";

Vue.directive("lazyloadit", {
  bind(el, binding, vnode) {},

  inserted(el: HTMLElement) {
    // function loadImage() {
    //   const imageElement = Array.from(el.children).find(
    //   el => el.nodeName === "IMG"
    //   );
    //   if (imageElement) {
    //     imageElement.addEventListener("load", () => {
    //       setTimeout(() => el.classList.add("loaded"), 100);
    //     });
    //     imageElement.addEventListener("error", () => console.log("error"));
    //     imageElement.src = imageElement.dataset.url;
    //   }
    // },

    function loadImage(el: HTMLElement) {
      // console.log("loading image ...");
      const imageElement =
        el.nodeName === "IMG" ? (el as HTMLImageElement) : null;
      // const imageElement = Array.from(el.children).find(
      //   el => el.nodeName === "IMG"
      // );
      if (imageElement) {
        // console.log("found img...");

        imageElement.addEventListener("load", () => {
          setTimeout(() => el.classList.add("loaded"), 100);
        });
        imageElement.addEventListener("error", () => console.log("error"));
        imageElement.src = imageElement.dataset.url as string;
      }
    }
    function handleIntersect(
      entries: IntersectionObserverEntry[],
      observer: IntersectionObserver
    ) {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          loadImage(el);
          observer.unobserve(el);
        }
      });
    }
    function createObserver(el: HTMLElement) {
      const options: IntersectionObserverInit = {
        root: null,
        threshold: 0
      };
      const observer = new IntersectionObserver(handleIntersect, options);
      observer.observe(el);
    }

    if (window["IntersectionObserver"]) {
      // console.log("creating observer ...");
      createObserver(el);
    } else {
      loadImage(el);
    }
  }
});
