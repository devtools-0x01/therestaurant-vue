module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: ["@nuxtjs/eslint-config-typescript", "plugin:nuxt/recommended"],
  plugins: [
    // 'prettier/vue',
    // 'plugin:prettier/recommended'
  ],
  // add your custom rules here
  rules: {
    quotes: ["error", "double"],
    semi: ["error", "always"],
    "vue/html-self-closing": ["error", "any"]
  }
};
