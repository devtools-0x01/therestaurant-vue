export const IMG_WIDTH = 380;
export const IMG_HEIGHT = 250;

export const foodCats = [
  {
    name: "Breakfast",
    img: {
      width: IMG_WIDTH,
      height: IMG_HEIGHT
    }
  },
  { name: "Lunch" },
  { name: "Snacks" },
  { name: "Dinner" },
  {
    name: "Desserts",
    img: {
      width: IMG_WIDTH,
      height: 700
    }
  }
];
