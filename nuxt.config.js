// alias: {
//   images: resolve(__dirname, "./assets/images"),
//   style: resolve(__dirname, "./assets/style"),
//   data: resolve(__dirname, "./assets/other/data")
// },
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s | The Restaurant",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [
      { rel: "preconnect", href: "https://https://fonts.gstatic.com" },
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      // {
      //   rel: "stylesheet",
      //   href:
      //     "https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&amp;family=Fira+Sans+Condensed:wght@700&amp;family=Fira+Sans:wght@300;400&amp;family=Lobster&amp;display=swap"
      // },
      {
        rel: "preload",
        as: "font",
        crossOrigin: "anonymous",
        href:
          "https://fonts.gstatic.com/s/firasanscondensed/v4/wEOsEADFm8hSaQTFG18FErVhsC9x-tarWU3IuMl0cjRYhY8XEA.woff2"
      },
      { rel: "stylesheet", href: "/bootstrap/dist/css/bootstrap.min.css" }
      // { rel: "stylesheet", href: "/css/styles.css" }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/static/css/styles.css"],

  router: {
    // linkActiveClass: "active"
  },

  loading: {
    color: "#f7ab1b"
  },

  // serverMiddleware: ["~/middleware/cache-control.js"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: "~/plugins/lazyloadit.client", name: "lazyloadit" }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build"
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};
